﻿var settings = {
  "OnlyLive": true,
  "Enable": false,
  "RemindInterval": 1800,
  "ReminderTime": "14:24-14:25",
  "ReminderMessage": "Heute Abend 18 Uhr Turnier! Jeder kann mitmachen! kobiqqLove",
  "HurryUpInterval": 600,
  "HurryUpTime": "14:25-14:26",
  "HurryUpMessage": "Nicht mehr lange dann geht das Turnier los! kobiqqLove",
  "ReminderTimeFrame": "15:00-17:00",
  "HurryUpTimeFrame": "17:00-18:01"
};