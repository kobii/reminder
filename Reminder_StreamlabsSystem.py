#!/usr/bin/python
# -*- coding: utf-8 -*-
# pylint: disable=invalid-name
#kf2103-sf-ua

#---------------------------------------
# Libraries and references
#---------------------------------------

import json
import os
import time
import codecs
import clr
import datetime

clr.AddReference("IronPython.SQLite.dll")
clr.AddReference("IronPython.Modules.dll")

#---------------------------------------
# [Required] Script information
#---------------------------------------

ScriptName = "RemindBot"
Website = "https://www.twitch.tv/kobiqq"
Creator = "[KobiQQ]"
Version = "1.02"
Description = "Remind the viewers in certain Intervalls"

#---------------------------------------
# Variables
#---------------------------------------
m_ConfigFile = os.path.join(os.path.dirname(__file__), "Settings/settings.json")
m_ConfigFileJs = os.path.join(os.path.dirname(__file__), "Settings/settings.js")

#---------------------------------------
# Classes Tries to load settings from file if given The 'default' variable names need to match UI_Config
#---------------------------------------
class Settings:
    """" Loads settings from file if file is found if not uses default values"""

    # The 'default' variable names need to match UI_Config
    def __init__(self, settingsFile=None):
        if settingsFile and os.path.isfile(settingsFile):
            with codecs.open(settingsFile, encoding='utf-8-sig', mode='r') as f:
                self.__dict__ = json.load(f, encoding='utf-8-sig')
                
        else: #set variables if no settings file
            self.OnlyLive = True
            self.Enable = False
            self.RemindInterval = 15
            self.ReminderTimeFrame = "11:00-12:00"
            self.ReminderMessage = "test message"
            self.HurryUpInterval = 1
            self.HurryUpTimeFrame = "12:00-12:05"
            self.HurryUpMessage = "hurry up"

    # Reload settings on save through UI
    def ReloadSettings(self, data):
        """Reload settings on save through UI"""
        self.__dict__ = json.loads(data, encoding='utf-8-sig')
        return

    # Save settings to files (json and js)
    def SaveSettings(self, settingsFile):
        """Save settings to files (json and js)"""
        with codecs.open(settingsFile, encoding='utf-8-sig', mode='w+') as f:
            json.dump(self.__dict__, f, encoding='utf-8-sig')
        with codecs.open(settingsFile.replace("json", "js"), encoding='utf-8-sig', mode='w+') as f:
            f.write("var settings = {0};".format(json.dumps(self.__dict__, encoding='utf-8-sig', ensure_ascii=False)))
        return

MySettings = Settings()
ReadMeFile = os.path.join(os.path.dirname(__file__), "ReadMe.txt")
CopyrightFile = os.path.join(os.path.dirname(__file__), "Copyright.txt")


#---------------------------------------
# [Required] functions
#---------------------------------------
def Init():

    global MySettings
    MySettings = Settings()

    global lastReminder
    lastReminder = time.time()

    global LastHurryUp
    LastHurryUp = time.time()

    if not os.path.isfile(m_ConfigFile):
        text_file = codecs.open(m_ConfigFile, encoding='utf-8-sig', mode='w')
        out = json.dumps(MySettings.__dict__, encoding="utf-8-sig")
        text_file.write(out)
        text_file.close()
    else:
        with codecs.open(m_ConfigFile,encoding='utf-8-sig', mode='r') as ConfigFile:
            MySettings.__dict__ = json.load(ConfigFile)

    if not os.path.isfile(m_ConfigFileJs):
        text_file = codecs.open(m_ConfigFileJs, encoding='utf-8-sig', mode='w')
        jsFile = "var settings =" + json.dumps(MySettings.__dict__, encoding="utf-8-sig") + ";"
        text_file.write(jsFile)
        text_file.close()

    # End of Init
    return

def Execute(data):
    """Required Execute function"""

    return

def Tick():
    """Required tick function"""
    if MySettings.Enable == True and (Parent.IsLive() or not MySettings.OnlyLive):

        global lastReminder
        global LastHurryUp

        if time.time() - lastReminder > MySettings.RemindInterval:

            lastReminder = time.time()
            now = datetime.datetime.now()

            ReminderStartTime = MySettings.ReminderTimeFrame.split('-')[0]
            ReminderEndTime = MySettings.ReminderTimeFrame.split('-')[1]
            ReminderStartTime = str(now.year) + "-" + str(now.month) + "-" + str(now.day) + " " + str(ReminderStartTime) 
            ReminderEndTime   = str(now.year) + "-" + str(now.month) + "-" + str(now.day) + " " + str(ReminderEndTime) 

            ReminderStartTime = datetime.datetime.strptime(ReminderStartTime, "%Y-%m-%d %H:%M")
            ReminderEndTime = datetime.datetime.strptime(ReminderEndTime,     "%Y-%m-%d %H:%M")
            

            if ReminderStartTime < now and ReminderEndTime > now:

                #Parent.Log(ScriptName,"Payout happend")
                Parent.SendStreamMessage (str(MySettings.ReminderMessage))

        
        elif time.time() - LastHurryUp > MySettings.HurryUpInterval:

            LastHurryUp = time.time()
            now = datetime.datetime.now()

            HurryUpStartTime = MySettings.HurryUpTimeFrame.split('-')[0]
            HurryUpEndTime = MySettings.HurryUpTimeFrame.split('-')[1]
            HurryUpStartTime = str(now.year) + "-" + str(now.month) + "-" + str(now.day) + " " + str(HurryUpStartTime) 
            HurryUpEndTime   = str(now.year) + "-" + str(now.month) + "-" + str(now.day) + " " + str(HurryUpEndTime) 

            HurryUpStartTime = datetime.datetime.strptime(HurryUpStartTime, "%Y-%m-%d %H:%M")
            HurryUpEndTime = datetime.datetime.strptime(HurryUpEndTime,     "%Y-%m-%d %H:%M")
            

            if HurryUpStartTime < now and HurryUpEndTime > now:

                #Parent.Log(ScriptName,"Payout happend")
                Parent.SendStreamMessage (str(MySettings.HurryUpMessage))

    return




def OpenReadMe():
	""" Open the script readme file in users default .txt application. """
	os.startfile(ReadMeFile)
	return

def OpenCopyright():
	""" Open the script readme file in users default .txt application. """
	os.startfile(CopyrightFile)
	return

def OpenStream():
	os.startfile("https://www.twitch.tv/kobiqq")
	return


def ReloadSettings(jsonData):
    MySettings.__dict__ = json.loads(jsonData)
    Parent.BroadcastWsEvent("EVENT_CURRENCY_RELOAD", jsonData)
    return

#---------------------------------------
# Reload Settings on Save
#---------------------------------------
def ReloadSettings(jsonData):
    # Globals
    global MySettings

    # Reload saved settings
    MySettings.ReloadSettings(jsonData)

    # End of ReloadSettings
    return

def UpdateSettings():
    with open(m_ConfigFile) as ConfigFile:
        MySettings.__dict__ = json.load(ConfigFile)
    return

#kf2103-ef-ua

